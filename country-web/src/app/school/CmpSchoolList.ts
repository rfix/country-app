import { Component, OnInit } from "@angular/core";
import { BaseListComponent } from "app/shared/BaseListComponent";
import {TableQuery} from '../shared/TabelQuery';
import { PrvSchool } from "./PrvSchool";
import { Router, ActivatedRoute } from "@angular/router";
import { TranslateService } from "@ngx-translate/core";
import { SrvMasterData } from "../shared/SrvMasterData";
import {MatDialog, MatDialogRef} from '@angular/material';
import { CmpSchoolForm } from "app/school/CmpSchoolForm";

@Component({
	templateUrl: './CmpSchoolList.html',
})

export class CmpSchoolList extends BaseListComponent implements OnInit{
	schoolForm: MatDialogRef<CmpSchoolForm>;
	selected = [];
	tableHeight: string;
	insertMode = false;
	lstSchool: any;
	canMaintain = false;
	noRowSelected = true;
	
	constructor(
		private prvSchool: PrvSchool,
		router: Router,
		route: ActivatedRoute,
		translate: TranslateService,
		private dialog: MatDialog,
		private srvMasterData: SrvMasterData) {
		super('CmpSchool', translate, route, router);
		this.canMaintain = this.srvMasterData.hasPermission('school:write');

		this.criteria = {
			schoolName: {value: '', label: 'schoolName'},
			email: {value: '', label: 'email'},
			address: {value: '', label: 'address'},
			cityId: {value: '', label: 'cityId'}
		};
	}

	openDialog(paramId): void {
		this.schoolForm = this.dialog.open(CmpSchoolForm,
			{
				data: {paramId: paramId},
				width: '600px',
				disableClose: true
			});
		this.schoolForm.afterClosed().subscribe(result => {
			if (result != '' && result != undefined) {
				this.PanictUtil.showAlertSuccess(this.translate.instant('data.saved'));
				this.reload();
			}
		});
	}

	ngOnInit(): void {
		this.tableHeight = (window.innerHeight - 245) + 'px';
		this.defaultOnInit();
	}

	add() {
		this.openDialog(0);
	}

	edit() {
		if (this.selected && this.selected.length > 0) {
			this.openDialog(this.selected[0].id);
		}
	}

	deleteSelected() {
		if (this.selected && this.selected.length > 0) {
			this.PanictUtil.showDialog(
				this.translate.instant('confirmation'),
				'<strong>' + this.translate.instant('delete.confirmation'),
				'confirmation',
				this.translate.instant('yes'),
				this.translate.instant('no'),
				'', (answer) => {
					if (answer === 'yes') {
						this.PanictUtil.showRequestIndicator();
						this.prvSchool.deleteRecord(this.selected[0].id).subscribe(
							data => this.reload(),
							error => {
								this.handleError(error, this.router);
							});
					}
				});
		}
	}

	getData(query: TableQuery) {
		return this.prvSchool.find(this.paginationObject.startIndex, this.paginationObject.maxRows, query);
		
	}
	
	exportData() {
		let query = this.getQueryObject();
		this.prvSchool.exportData(this.paginationObject.startIndex, this.paginationObject.maxRows, query).subscribe((res) => {
			window['saveAs'](res, 'school.xlsx');
		}, error => {
			this.handleError(error, this.router);
		});
	}
}