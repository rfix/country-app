import { Injectable } from "@angular/core";
import { BaseService } from "app/shared/base.service";
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { TableQuery } from "app/shared/TabelQuery";


@Injectable()
export class PrvSchool extends BaseService {
    constructor(private http: HttpClient) {
		super();
    }
    
    find(start: number, max: number, tq: TableQuery) {
      return this.http.post(this.apiUrl + 'school?start=' + start + '&max=' + max, JSON.stringify(tq));
    }
  
    findById(id: number) {
      return this.http.get(this.apiUrl + 'school/' + id);
    }
  
    save(id: number, data: any) {
      return this.http.put(this.apiUrl + 'school/' + id, JSON.stringify(data));
    }
    deleteRecord(id) {
      return this.http.delete(this.apiUrl + 'school/' + id);
    }
    exportData(start: number, max: number, tq: TableQuery) {
      return this.http.post(this.apiUrl + 'school/xls?start=' + start + '&max=' + max, JSON.stringify(tq), {
        responseType: 'blob'
      });
    }

}