import {NgModule} from '@angular/core';
import {CmpSchoolList} from './CmpSchoolList';
import {RoutSchool} from './RoutSchool';
import {SharedModule} from '../shared/shared.module';
import {PrvSchool} from './PrvSchool';
import { CmpSchoolForm } from './CmpSchoolForm';

@NgModule({
	imports: [RoutSchool, SharedModule],
	declarations: [CmpSchoolList, CmpSchoolForm],
	providers:[PrvSchool]
})
export class MdlSchoolList {}