import {Component, OnInit, ViewChild, Inject} from '@angular/core';
import {BaseComponent} from '../shared/base.component';
import {Router} from '@angular/router';
import {PrvSchool} from './PrvSchool';
import {NgForm} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';

class Model {
	id: number;
	schoolName: string;
	email: string;
	address: string;
	cityId: number;
}

@Component({
	selector: 'school-form',
	templateUrl: './CmpSchoolForm.html',
})

export class CmpSchoolForm extends BaseComponent implements OnInit {
	
	id: number;
	insertMode: boolean = false;
	model: Model;
	myForm: NgForm;
	@ViewChild('myForm') currentForm: NgForm;
	saving = false;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data: any,
		private dialogRef: MatDialogRef<CmpSchoolForm>,
		private prvSchool: PrvSchool,
		private router: Router,
		translate: TranslateService) {
		super(translate);
	}

	ngOnInit() {
		console.log('onInit');
		this.id = this.data.paramId;
		this.insertMode = this.id === 0;
		if (!this.insertMode) {
			this.PanictUtil.showRequestIndicator();
			this.prvSchool.findById(this.id).subscribe(
				(data: Model) => {
					this.model = data;
					this.PanictUtil.hideRequestIndicator();
				},
				(error) => this.handleError(error, this.router)
			);
		} else {
			this.model = new Model;
			this.model.id = this.id;
		}
	}

	submit() {
		if (this.saving) return;
		this.PanictUtil.showRequestIndicator();
		this.saving = true;
		this.prvSchool.save(this.model.id, this.model).subscribe(
			(data: any) => {
				this.PanictUtil.hideRequestIndicator();
				this.saving = false;
				this.insertMode = false;
				this.dialogRef.close(this.model);
			},
			error => {
				this.handleError(error, this.router);
				this.saving = false;
			}
		)
	}
	
	close() {
		this.dialogRef.close();
	}
	
	formErrors = {
		'schoolName': '',
	};

	validationMessages = {
		'schoolName': {
			'required': {key: 'this.field.is.required'},
			'maxlength': {key: 'max.char', param: {par1: '255'}},
			'minlength': {key: 'min.char', param: {par1: '4'}},
		},
	};


}