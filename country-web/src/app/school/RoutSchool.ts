import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CmpSchoolList} from './CmpSchoolList';
import { CmpSchoolForm } from './CmpSchoolForm';

@NgModule({
	imports: [
		RouterModule.forChild([
			{path: ':id', component: CmpSchoolForm},
			{path: '', component: CmpSchoolList},
		])
	],
})
export class RoutSchool {}