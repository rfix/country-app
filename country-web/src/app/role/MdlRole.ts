import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import {CmpRoleList} from './CmpRoleList';
import {CmpRoleForm} from './CmpRoleForm';
import {RoutRole} from './RoutRole';
import {PrvRole} from './PrvRole';
@NgModule({
	imports: [RoutRole, SharedModule],
	declarations: [CmpRoleList, CmpRoleForm],
	providers:[PrvRole]
})
export class MdlRole { }
