import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {RoutCity} from './RoutCity';
import {CmpCityList} from './CmpCityList';
import {CmpCityForm} from './CmpCityForm';
import {PrvCity} from './PrvCity';


@NgModule({
	imports: [RoutCity, SharedModule],
	declarations: [CmpCityList, CmpCityForm],
	providers: [PrvCity]
})
export class MdlCity {}