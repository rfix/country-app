import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute} from '@angular/router';
import {TableQuery, TableCriteria} from '../shared/TabelQuery';
import {BaseListComponent} from '../shared/BaseListComponent';
import {PrvCity} from './PrvCity';
import {TranslateService} from '@ngx-translate/core';
import {SrvMasterData} from '../shared/SrvMasterData';

@Component({
	templateUrl: './CmpCityList.html',
})

export class CmpCityList extends BaseListComponent implements OnInit {

	selected = [];
	tableHeight: string;
	insertMode = false;
	lstContinent = ['Asia', 'Europe', 'Africa', 'Oceania', 'North America', 'Antarctica', 'South America', 'Australia'];
	lstCountry: any;
	canMaintain = false;

	constructor(
		private prvCity: PrvCity,
		router: Router,
		route: ActivatedRoute,
		translate: TranslateService,
		private srvMasterData: SrvMasterData) {
		super('CmpCity', translate, route, router);
		this.canMaintain = this.srvMasterData.hasPermission('city:write');

		this.criteria = {
			name: {value: '', label: 'city.name'},
			country: {value: '', label: 'country'},
			continent: {value: '', label: 'continent'},
		};
	}

	ngOnInit() {
		this.tableHeight = (window.innerHeight - 245) + 'px';
		this.defaultOnInit();
		this.getLstCountryByContinent();
	}

	getData(query: TableQuery) {
		return this.prvCity.find(this.paginationObject.startIndex, this.paginationObject.maxRows, query);
	}

	getLstCountryByContinent() {
		this.PanictUtil.showRequestIndicator();
		this.prvCity.findCountry(this.criteria.continent.value).subscribe(
			(data: any) => {
				this.lstCountry = data;
				this.PanictUtil.hideRequestIndicator();
			}
		);
	}


	edit() {
		if (this.selected && this.selected.length > 0) {
			this.router.navigate(['../' + this.selected[0].id], {relativeTo: this.route});
		}
	}
	deleteSelected() {
		if (this.selected && this.selected.length > 0) {
			this.PanictUtil.showDialog(
				this.translate.instant('confirmation'),
				'<strong>' + this.translate.instant('delete.confirmation'),
				'confirmation',
				this.translate.instant('yes'),
				this.translate.instant('no'),
				'', (answer) => {
					if (answer === 'yes') {
						this.PanictUtil.showRequestIndicator();
						this.prvCity.deleteRecord(this.selected[0].id).subscribe(
							data => this.reload(),
							error => {
								this.handleError(error, this.router);
							});
					}
				});
		}
	}
	
	exportData() {
		let query = this.getQueryObject();
		this.prvCity.exportData(this.paginationObject.startIndex, this.paginationObject.maxRows, query).subscribe((res) => {
			window['saveAs'](res, 'city.xlsx');
		}, error => {
			this.handleError(error, this.router);
		});
	}
}