import { NgModule } from '@angular/core';
import { CmpUserList } from './CmpUserList';
import { CmpUserForm } from './CmpUserForm';
import { RoutUser} from './RoutUser';
import { SharedModule } from '../shared/shared.module';

@NgModule({
	imports: [RoutUser, SharedModule],
	declarations: [CmpUserList, CmpUserForm],
})
export class MdlUser { }
