import { Component, Input, AfterViewInit, ViewChild, Output, EventEmitter, OnChanges, SimpleChange } from '@angular/core';

export interface MessageObject {
	title: string;
	content: string;
	type: string;
	yesLabel?: string;
	noLabel?: string;
	cancelLabel?: string;
	onClose?: any;
}

declare var jQuery: any;

@Component({
	selector: 'b-dialog',
	template: `
<div #bModal class="modal" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" [class.text-info]="type=='info'" [class.text-warning]="type=='confirmation'" [class.text-danger]="type=='error'">
        <h4 class="modal-title">{{title}}</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <ng-content></ng-content>
    </div>
  </div>
</div>
`,
})

/**
 * Bootstrap alert component. Possible value of type is success, info, warning and danger
 */
export class DialogComponent implements AfterViewInit, OnChanges {
	@ViewChild('bModal') bModal: any;
	@Input() title: string;
	@Input() shown: boolean;
	@Output() closed: EventEmitter<any> = new EventEmitter();
	@Input() type: any;
	private objRef: any;

	ngAfterViewInit() {
		this.objRef = jQuery(this.bModal.nativeElement);
		this.objRef.on('hidden.bs.modal', (e: any) => {
			this.closed.emit('closed');
		});
	}

	ngOnChanges(changes: { [propName: string]: SimpleChange }) {
		if (!this.objRef) {
			return;
		}
		if (changes['shown']) {
			if (changes['shown'].currentValue) {
				this.objRef.modal('show');
			} else {
				this.objRef.modal('hide');
			}
		}
	}
}