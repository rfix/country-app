import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CmpHome} from './CmpHome';
import {CmpDashboard} from 'app/dashboard/CmpDashboard';
import {CmpHelp} from 'app/home/CmpHelp';
import {PageNotFoundComponent} from '../not-found.component';
import {AuthGuard} from '../login/auth-guard.service';

@NgModule({
	imports: [RouterModule.forChild([
		{
			path: 'home', component: CmpHome,
			canActivate: [AuthGuard],
			children: [
				{path: 'user', loadChildren: 'app/user/MdlUser#MdlUser'},
				{path: 'role', loadChildren: 'app/role/MdlRole#MdlRole'},
				{path: 'country', loadChildren: 'app/country/MdlCountry#MdlCountry'},
				{path: 'city', loadChildren: 'app/city/MdlCity#MdlCity'},
				{path: 'util', loadChildren: 'app/util/MdlUtil#MdlUtil'},
				{path: 'school', loadChildren: 'app/school/MdlSchoolList#MdlSchoolList'},
				{path: 'help', component: CmpHelp},
				{path: '', component: CmpDashboard},
			]
		},
		{path: '**', component: PageNotFoundComponent},
	])],
	exports: [RouterModule]
})
export class RoutHome {}
