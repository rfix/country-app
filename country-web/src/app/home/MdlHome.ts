import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CmpHome} from './CmpHome';
import {CmpHelp} from './CmpHelp';
import {RoutHome} from './RoutHome';
import {SharedModule} from '../shared/shared.module';

@NgModule({
	imports: [CommonModule, RoutHome, SharedModule],
	declarations: [CmpHome, CmpHelp],
	exports: [CmpHome],
	providers: []
})
export class MdlHome {}
