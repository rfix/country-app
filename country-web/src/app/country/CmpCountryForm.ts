import {Component, OnInit, ViewChild, Inject} from '@angular/core';
import {BaseComponent} from '../shared/base.component';
import {Router} from '@angular/router';
import {PrvCountry} from './PrvCountry';
import {NgForm} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';

class Model {
	id: number;
	name: string;
	capital: string;
	continent: string;
	independence: string;
	population: number;
}

@Component({
	selector: 'country-form',
	templateUrl: './CmpCountryForm.html',
})

export class CmpCountryForm extends BaseComponent implements OnInit {
	id: number;
	insertMode: boolean = false;
	model: Model;
	myForm: NgForm;
	lstContinent = ['Asia', 'Europe', 'Africa', 'Oceania', 'North America', 'Antarctica', 'South America', 'Australia'];
	@ViewChild('myForm') currentForm: NgForm;
	saving = false;

	constructor(
		@Inject(MAT_DIALOG_DATA) public data: any,
		private dialogRef: MatDialogRef<CmpCountryForm>,
		private prvCountry: PrvCountry,
		private router: Router,
		translate: TranslateService) {
		super(translate);
	}
	ngOnInit() {
		console.log('onInit');
		this.id = this.data.paramId;
		this.insertMode = this.id === 0;
		if (!this.insertMode) {
			this.PanictUtil.showRequestIndicator();
			this.prvCountry.findById(this.id).subscribe(
				(data: Model) => {
					this.model = data;
					this.PanictUtil.hideRequestIndicator();
				},
				(error) => this.handleError(error, this.router)
			);
		} else {
			this.model = new Model;
			this.model.id = this.id;
		}
	}

	ngAfterViewChecked() {
		this.formChanged();
	}

	formChanged() {
		if (this.currentForm === this.myForm) {return;}
		this.myForm = this.currentForm;
		if (this.myForm) {
			this.myForm.valueChanges
				.subscribe(data => this.onValueChanged(data));
		}
	}

	onValueChanged(data?: any) {
		if (!this.myForm) {return;}
		const form = this.myForm.form;

		for (const field in this.formErrors) {
			// clear previous error message (if any)
			this.formErrors[field] = '';
			const control = form.get(field);

			if (control && control.dirty && !control.valid) {
				const messages = this.validationMessages[field];
				for (const key in control.errors) {
					this.formErrors[field] = messages[key];
				}
			}
		}
	}
	
	submit() {
		if (this.saving) return;
		this.PanictUtil.showRequestIndicator();
		this.saving = true;
		this.prvCountry.save(this.model.id, this.model).subscribe(
			(data: any) => {
				this.PanictUtil.hideRequestIndicator();
				this.saving = false;
				this.insertMode = false;
				this.dialogRef.close(this.model);
			},
			error => {
				this.handleError(error, this.router);
				this.saving = false;
			}
		)
	}
	
	close() {
		this.dialogRef.close();
	}
	
	formErrors = {
		'name': '',
	};

	validationMessages = {
		'name': {
			'required': {key: 'this.field.is.required'},
			'maxlength': {key: 'max.char', param: {par1: '255'}},
			'minlength': {key: 'min.char', param: {par1: '4'}},
		},
	};
}