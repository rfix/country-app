import {Component, OnInit} from '@angular/core';
import {PrvCountry} from './PrvCountry';
import {Router, ActivatedRoute} from '@angular/router';
import {TableQuery} from '../shared/TabelQuery';
import {BaseListComponent} from '../shared/BaseListComponent';

import {MatDialog, MatDialogRef} from '@angular/material';
import {CmpCountryForm} from '../country/CmpCountryForm';
import {TranslateService} from '@ngx-translate/core';
import {SrvMasterData} from '../shared/SrvMasterData';

@Component({
	templateUrl: './CmpCountryList.html',
})

export class CmpCountryList extends BaseListComponent implements OnInit {
	countryForm: MatDialogRef<CmpCountryForm>;

	selected = [];
	noRowSelected = true;
	tableHeight: string;

	saving = false;
	insertMode = false;
	canMaintain = false;


	constructor(private prvCountry: PrvCountry,
		router: Router,
		route: ActivatedRoute,
		private dialog: MatDialog,
		private srvMasterData: SrvMasterData,
		translate: TranslateService) {
		super('CmpCountry', translate, route, router);
		this.canMaintain = this.srvMasterData.hasPermission('country:write');
		this.criteria = {
			name: {value: '', label: 'country.name'},
			capital: {value: '', label: 'capital.city'},
			continent: {value: '', label: 'continent'},
			independence: {value: '', label: 'independence.day'},
			population: {value: '', label: 'population'},
		};
	}

	openDialog(paramId): void {
		this.countryForm = this.dialog.open(CmpCountryForm,
			{
				data: {paramId: paramId},
				width: '600px',
				disableClose: true
			});
		this.countryForm.afterClosed().subscribe(result => {
			if (result != '' && result != undefined) {
				this.PanictUtil.showAlertSuccess(this.translate.instant('data.saved'));
				this.reload();
			}
		});
	}

	ngOnInit() {
		this.tableHeight = (window.innerHeight - 245) + 'px';
		super.defaultOnInit();
	}

	edit() {
		if (this.selected && this.selected.length > 0) {
			this.openDialog(this.selected[0].id);
		}
	}
	add() {
		this.openDialog(0);
	}
	

	deleteSelected() {
		if (this.selected && this.selected.length > 0) {
			this.PanictUtil.showDialog(
				this.translate.instant('confirmation'),
				'<strong>' + this.translate.instant('delete.confirmation'),
				'confirmation',
				this.translate.instant('yes'),
				this.translate.instant('no'),
				'', (answer) => {
					if (answer === 'yes') {
						this.PanictUtil.showRequestIndicator();
						this.prvCountry.deleteRecord(this.selected[0].id).subscribe(
							data => this.reload(),
							error => {
								this.handleError(error, this.router);
							});
					}
				});
		}
	}

	getData(query: TableQuery) {
		return this.prvCountry.find(this.paginationObject.startIndex, this.paginationObject.maxRows, query);
	}

	exportData() {
		let query = this.getQueryObject();
		this.prvCountry.exportData(this.paginationObject.startIndex, this.paginationObject.maxRows, query).subscribe((res) => {
			window['saveAs'](res, 'country2.xlsx');
		}, error => {
			this.handleError(error, this.router);
		});
	}
}
