import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CmpCountryList} from './CmpCountryList';
import {CmpCountryForm} from './CmpCountryForm';

@NgModule({
	imports: [
		RouterModule.forChild([
			{path: ':id', component: CmpCountryForm},
			{path: '', component: CmpCountryList},
		])
	],
})
export class RoutCountry {}