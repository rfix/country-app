import {NgModule} from '@angular/core';
import {CmpCountryList} from './CmpCountryList';
import {CmpCountryForm} from './CmpCountryForm';
import {RoutCountry} from './RoutCountry';
import {SharedModule} from '../shared/shared.module';
import {PrvCountry} from './PrvCountry';

@NgModule({
	imports: [RoutCountry, SharedModule],
	declarations: [CmpCountryList, CmpCountryForm],
	providers:[PrvCountry]
})
export class MdlCountry {}