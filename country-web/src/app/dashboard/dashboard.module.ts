import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CmpDashboard} from './CmpDashboard';
import {DashboardRoutingModule} from './dashboard-routing.module';
import {SharedModule} from '../shared/shared.module';

@NgModule({
	imports: [CommonModule, DashboardRoutingModule, SharedModule],
	declarations: [CmpDashboard],
	exports: [CmpDashboard],
	providers: []
})

export class DashboardModule {}
