import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {BaseComponent} from '../shared/base.component';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
@Component({
	selector: 'dashboard',
	templateUrl: 'CmpDashboard.html',
})

export class CmpDashboard extends BaseComponent implements OnInit {
	lstPolda: any;
	lstPolres: any;
	polda = "";
	polres = "";
	years: number[] = [];
	tahun: number;
	prefix = 'CmpDashboard';
	parameter: any;
	@Output() reload: EventEmitter<any> = new EventEmitter();
	constructor(
		private router: Router,
		translate: TranslateService) {
		super(translate);
		var today = new Date();
		var yyyy = today.getFullYear();
		for (var tahun = 2017; tahun <= yyyy; tahun++) {
			this.years.push(tahun);
		}

		if (localStorage.getItem(this.prefix + "_" + 'tahun')) {
			this.tahun = +localStorage.getItem(this.prefix + "_" + 'tahun');
		} else {
			this.tahun = yyyy;
		}
	}

	ngOnInit() {

	}

	reloadChart() {
		if (this.polda == null) {
			this.polda = "";
		}
		localStorage.setItem(this.prefix + "_" + 'polda', this.polda);
		localStorage.setItem(this.prefix + "_" + 'polres', this.polres);
		localStorage.setItem(this.prefix + "_" + 'tahun', this.tahun + '');
		this.reload.emit({polda: this.polda, polres: this.polres, tahun: this.tahun});
		this.parameter = {polda: this.polda, polres: this.polres, tahun: this.tahun};
	}

}
