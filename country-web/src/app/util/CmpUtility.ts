import {Component, AfterViewInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {BaseComponent} from '../shared/base.component';
import {PrvCity} from '../city/PrvCity';
import {Router} from '@angular/router';

@Component({
	selector: 'p-error-handling',
	templateUrl: 'CmpUtility.html',
})

export class CmpUtility extends BaseComponent implements AfterViewInit {
	requestIndicatorClicked = false;
	alertSuccessClicked = false;
	alertDangerClicked = false;
	alertWarningClicked = false;
	alertInfoClicked = false;
	errorMsg = '';

	constructor(translate: TranslateService,
		private prvCity: PrvCity,
		private router: Router) {
		super(translate);
	}

	ngAfterViewInit() {
	}

	showRequestIndicator() {
		this.requestIndicatorClicked = true;
		this.PanictUtil.showRequestIndicator();
	}

	hideRequestIndicator() {
		this.requestIndicatorClicked = false;
		this.PanictUtil.hideRequestIndicator();
	}

	showAlertSuccess() {
		this.alertSuccessClicked = true;
		this.PanictUtil.showAlertSuccess(this.translate.instant('alert.success'));
	}

	hideAlertSuccess() {
		this.alertSuccessClicked = false;
		this.PanictUtil.hideAlert();
	}

	showAlertDanger() {
		this.alertDangerClicked = true;
		this.PanictUtil.showAlertDanger(this.translate.instant('alert.danger'));
	}

	hideAlertDanger() {
		this.alertDangerClicked = false;
		this.PanictUtil.hideAlert();
	}

	showAlertWarning() {
		this.alertWarningClicked = true;
		this.PanictUtil.showAlertWarning(this.translate.instant('alert.warning'));
	}

	hideAlertWarning() {
		this.alertWarningClicked = false;
		this.PanictUtil.hideAlert();
	}

	showAlertInfo() {
		this.alertInfoClicked = true;
		this.PanictUtil.showAlertInfo(this.translate.instant('alert.info'));
	}

	hideAlertInfo() {
		this.alertInfoClicked = false;
		this.PanictUtil.hideAlert();
	}

	errorNoParam() {
		this.prvCity.errorNoParam().subscribe(
			data => {},
			error => this.handleError(error, this.router)
		);
	}

	errorWithParam() {
		this.prvCity.errorWithParam().subscribe(
			data => {},
			error => this.handleError(error, this.router)
		);
	}
}
