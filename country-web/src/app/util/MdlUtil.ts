import {NgModule} from '@angular/core';
import {SharedModule} from '../shared/shared.module';
import {RoutUtil} from './RoutUtil';
import {CmpUtility} from './CmpUtility';
import {PrvCity} from '../city/PrvCity'


@NgModule({
	imports: [RoutUtil, SharedModule],
	declarations: [CmpUtility],
	providers: [PrvCity]
})
export class MdlUtil {}