package com.panemu.country.common;

import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.jdbc.JdbcRealm;
import org.apache.shiro.subject.Subject;

/**
 *
 * @author amrullah
 */
public class SaltedJdbcRealm extends JdbcRealm {

	private static SaltedJdbcRealm instance;
	public SaltedJdbcRealm() {
		instance = this;
		setSaltStyle(SaltStyle.COLUMN);
	}
	
	public AuthorizationInfo getSubjectInfo(Subject subject) {
		return this.doGetAuthorizationInfo(subject.getPrincipals());
	}
	
	public static SaltedJdbcRealm getInstance() {
		return instance;
	}
	
}
