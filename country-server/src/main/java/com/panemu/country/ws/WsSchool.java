/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panemu.country.ws;

import com.panemu.country.common.CommonUtil;
import com.panemu.country.common.TableData;
import com.panemu.country.dto.DtoCity;
import com.panemu.country.dto.DtoSchool;
import com.panemu.country.error.ErrorCode;
import com.panemu.country.error.ErrorEntity;
import com.panemu.country.error.GeneralException;
import com.panemu.country.rcd.School;
import com.panemu.country.rpt.RptCityList;
import com.panemu.country.rpt.RptSchool;
import com.panemu.country.srv.SrvSchool;
import com.panemu.search.TableCriteria;
import com.panemu.search.TableQuery;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author imam
 */
@Path("/school")
@Produces(MediaType.APPLICATION_JSON)
@Stateless
public class WsSchool {
	
	private Logger log = LoggerFactory.getLogger(WsSchool.class);
	
	@Inject
	private SrvSchool srvSchool;
	
	@POST
	@RequiresPermissions(value = {"school:read", "school:write"}, logical = Logical.OR)
	public TableData<School> findAll(
				@QueryParam("start") int startIndex, 
				@QueryParam("max") int maxRecord, 
				TableQuery tq) {
		TableData<School> result = srvSchool.findSchool(tq, startIndex, maxRecord);
		result.getRows().forEach(rcd -> {
			srvSchool.detach(rcd);
		});
		return result;
	}
	
	@GET
	@Path("{id}")
	@RequiresPermissions(value = {"school:read", "school:write"}, logical = Logical.OR)
	public DtoSchool getSchool(@PathParam("id") int id) {
		DtoSchool dto = null;
		if (id > 0) {
			School rcd = srvSchool.findById(School.class, id);
			dto = DtoSchool.create(rcd);
		}
		return dto;
	}	
	
@DELETE
@Path("{id}")
@RequiresPermissions(value = {"school:write"})
public void delete(@PathParam("id") Integer id) {
	School rcd = srvSchool.findById(School.class, id);
	srvSchool.deleteSchool(rcd);
}

@PUT	
@Path("{id}")
@RequiresPermissions(value = {"school:write"})
public void saveSchool(@PathParam("id") Integer id, DtoSchool dto){
		School rcd = null;
		if(id > 0){
			rcd = srvSchool.findById(School.class, id);
		} else {
			rcd = new School();
		}
		rcd.setSchoolName(dto.schoolName);
		rcd.setEmail(dto.email);
		rcd.setCityId(dto.cityId);
		rcd.setAddress(dto.address);
		if(id > 0){
			srvSchool.updateSchool(rcd);
		} else {
			srvSchool.insertSchool(rcd);
		}
	}
	
	@POST
	@Path("xls")
	@RequiresPermissions(value = {"school:read", "school:write"}, logical = Logical.OR)
	public Response export(
			  @QueryParam("start") int startIndex,
			  @QueryParam("max") int maxRecord,
			  TableQuery tq) {
		TableData<School> data = this.findAll(startIndex, maxRecord, tq);
		RptSchool rpt = new RptSchool(data, startIndex, maxRecord);
		return CommonUtil.buildExcelResponse(rpt, "school");
	}
	
	@GET
	@Path("errornoparam")
	public void testError() {
		throw GeneralException.create(new ErrorEntity(ErrorCode.ER0001, null));
	}
	
	@GET
	@Path("errorwithparam")
	public void testError2() {
		String name = "Tomi";
		int age = 25;
		throw GeneralException.create(new ErrorEntity(ErrorCode.ER0002, name, age+""));
	}
	
}
