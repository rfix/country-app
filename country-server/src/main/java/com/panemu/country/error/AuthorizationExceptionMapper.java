package com.panemu.country.error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import org.apache.commons.lang.StringUtils;
import org.apache.shiro.authz.AuthorizationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amrullah
 */
@Provider
public class AuthorizationExceptionMapper implements ExceptionMapper<AuthorizationException> {

	private Logger log = LoggerFactory.getLogger(AuthorizationExceptionMapper.class);

	@Override
	public Response toResponse(AuthorizationException exception) {
		log.error("authentication exception: " + exception.getMessage());
		ErrorEntity error = new ErrorEntity();
		if (StringUtils.contains(exception.getMessage(), "not authenticated")
				  || StringUtils.contains(exception.getMessage(), "subject is anonymous")) {
			error.setCode(ErrorCode.ER0401);
			return Response.status(Response.Status.UNAUTHORIZED).entity(error).type(MediaType.APPLICATION_JSON).build();
		} else {
			//Subject does not have permission [x:y]
			error.setCode(ErrorCode.ER0403);
			return Response.status(Response.Status.FORBIDDEN).entity(error).type(MediaType.APPLICATION_JSON).build();
		}
	}

}
