package com.panemu.country.error;

import javax.ejb.ApplicationException;
import javax.ws.rs.BadRequestException;
import javax.ws.rs.core.Response;

/**
 *
 * @author amrullah
 */
@ApplicationException(rollback = true)
public class KnownException extends BadRequestException {

	public KnownException(Response response) {
		super(response);
	}
	
}
