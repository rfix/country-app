package com.panemu.country.error;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author amrullah
 */
public class GeneralException {
	private static Logger log = LoggerFactory.getLogger(GeneralException.class);
	private GeneralException(){}
	
	public static KnownException create(ErrorEntity errorEntity) {
		log.error(errorEntity.getMessage());
		final KnownException badRequestException = new KnownException(Response.status(Response.Status.BAD_REQUEST)
				  .entity(errorEntity).type(MediaType.APPLICATION_JSON).build());
		return badRequestException;
	}
	
}
