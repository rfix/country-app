package com.panemu.country.dto;

import java.util.Collection;

/**
 *
 * @author amrullah
 */
public class DtoAuth {

	private int id;
	private String username;
	private String email;
	private String role;
	private Collection<String> permissions;
	private boolean capital;
	private boolean price;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Collection<String> getPermissions() {
		return permissions;
	}

	public void setPermissions(Collection<String> permissions) {
		this.permissions = permissions;
	}

	public boolean getCapital() {
		return capital;
	}

	public void setCapital(boolean lowerCase) {
		this.capital = lowerCase;
	}

	public boolean isPrice() {
		return price;
	}

	public void setPrice(boolean price) {
		this.price = price;
	}
}
