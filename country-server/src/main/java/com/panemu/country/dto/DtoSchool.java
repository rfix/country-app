/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panemu.country.dto;

import com.panemu.country.rcd.School;

/**
 *
 * @author imam
 */
public class DtoSchool {
	
	public int id;
	public String schoolName;
	public String address;
	public String email;
	public int cityId;
	public String cityName;
	
	public static DtoSchool create(School rcd){
		DtoSchool dto = new DtoSchool();
		dto.id = rcd.getId();
		dto.schoolName = rcd.getSchoolName();
//		dto.cityName = rcd.getCity().getName();
		dto.address = rcd.getAddress();
		dto.email = rcd.getEmail();
		dto.cityId = rcd.getCityId();

		if(rcd.getCity() != null) {
			dto.cityName = rcd.getCity().getName();
		}
		return dto;
	}
	
}
