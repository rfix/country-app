/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.panemu.country.srv;

import com.panemu.country.common.TableData;
import com.panemu.country.error.ErrorCode;
import com.panemu.country.error.ErrorEntity;
import com.panemu.country.error.GeneralException;
import com.panemu.country.rcd.School;
import com.panemu.search.InvalidSearchCriteria;
import com.panemu.search.SortingInfo;
import com.panemu.search.TableCriteria;
import com.panemu.search.TableQuery;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author imam
 */
@Stateless
public class SrvSchool extends SrvBase{
	
	private static Logger log = LoggerFactory.getLogger(SrvSchool.class);
	@PersistenceContext
	protected EntityManager em;
	
	public School insertSchool(School rcd) {
		return super.insert(rcd);
	}

	public School updateSchool(School rcd) {
		return super.update(rcd);
	}

	public void deleteSchool(School rcd) {
		super.delete(rcd);
	}

	public TableData<School> findSchool(TableQuery query, int startIndex, int maxRecord){
		try {
			for(TableCriteria crit : query.getTableCriteria()){
				crit.setTableAlias("con");
				if ("school".equals(crit.getAttributeName())) {
					crit.setAttributeName("id");
					crit.setSearchModeToInt();
				} else if("cityId".equals(crit.getAttributeName())){
					crit.setAttributeName("cityId");
					crit.setSearchModeToInt();
				} 
			}
			
			for(SortingInfo si : query.getSortingInfos()){
				si.setTableAlias("con");
			}
			
			String whereClause = query.generateWhereClause(true);
			String orderClause = query.generateOrderByClause(true);
			
			TypedQuery<Long> countQuery = em.createQuery("SELECT count(con) from School con " + whereClause, Long.class);
			query.applyParameter(countQuery);
			long totalRow = countQuery.getSingleResult();
			
			TypedQuery<School> typedQuery = em.createQuery("SELECT con from School con " + whereClause + orderClause, School.class);
			query.applyParameter(typedQuery);
			typedQuery.setFirstResult(startIndex);
			if (maxRecord > 0) {
				typedQuery.setMaxResults(maxRecord);
			}
			List<School> resultList = typedQuery.getResultList();
			TableData<School> td = new TableData<>(resultList, totalRow);
			return td;
		} catch (InvalidSearchCriteria ex) {
			throw GeneralException.create(new ErrorEntity(ErrorCode.ER0010, ex.getColumnName(), ex.getValue()));
		} catch (Exception ex) {
			log.error(ex.getMessage(), ex);
			throw GeneralException.create(new ErrorEntity(ErrorCode.ER0400, ex.getClass().getSimpleName()));
		}
	}
	
//	public TableData<School> findSchoolByCity(TableQuery query, int startIndex, int maxRecord){
//		try {
//			for(TableCriteria crit : query.getTableCriteria()){
//				crit.setTableAlias("cit");
//				if("city".equals(crit.getAttributeName())){
//					crit.setAttributeName("city.id");
//					crit.setSearchModeToInt();
//				}
//			}
//			
//			for(SortingInfo si : query.getSortingInfos()){
//				si.setTableAlias("cit");
//				if("cityName".equals(si.getAttributeName())){
//					si.setAttributeName("city.name");
//				} 
//			}
//			
//			String whereClause = query.generateWhereClause(true);
//			String orderClause = query.generateOrderByClause(true);
//			TypedQuery<Long> countQuery = em.createQuery("SELECT count(cit) from School cit left join cit.country eo " + whereClause, Long.class);
//			query.applyParameter(countQuery);
//			long totalRow = countQuery.getSingleResult();
//			
//			TypedQuery<School> typedQuery = em.createQuery("SELECT cit from School cit left join cit.country eo " + whereClause + orderClause, School.class);
//			query.applyParameter(typedQuery);
//			typedQuery.setFirstResult(startIndex);
//			if (maxRecord > 0) {
//				typedQuery.setMaxResults(maxRecord);
//			}
//			List<School> resultList = typedQuery.getResultList();
//			TableData<School> td = new TableData<>(resultList, totalRow);
//			return td;
//		} catch (InvalidSearchCriteria ex) {
//			throw GeneralException.create(new ErrorEntity(ErrorCode.ER0010, ex.getColumnName(), ex.getValue()));
//		} catch (Exception ex) {
//			log.error(ex.getMessage(), ex);
//			throw GeneralException.create(new ErrorEntity(ErrorCode.ER0400, ex.getClass().getSimpleName()));
//		}
//	}
	
}
